<?php
 require_once 'php/conexao.php';
    class Contato{
        private $nome;
        private $telefone;
        private $email; 

        function Contatos(){
            $this->preparaContatos();
        }
        private function preparaContatos(){
            $this->nome_contato = isset($_POST['nome_contato']);
            $this->telefone_contato = isset($_POST['telefone_contato']);
            $this->email_contato = isset($_POST['email_contato']);
        }
        public function getNome (){
            return $this->nome;
        }
        public function setNome($nome) {
          $this->nome = $nome;
        }
        public function getTelefone (){
            return $this->telefone;
        }
        public function setTelefone($telefone) {
          $this->telefone = $telefone;
        }
        public function getEmail (){
            return $this->email;
        }
        public function setEmail($email) {
          $this->email = $email;
        }	
        function cadastra_contato(){
            $sql = "INSERT INTO contatos_gh(nome, telefone, email) VALUES (?, ?, ?)";
            $stmt = Conexao::getInstance()->prepare($sql);
            $stmt->bindParam(1,$this->nome);
            $stmt->bindParam(2,$this->telefone);
            $stmt->bindParam(3,$this->email);
            $stmt->execute();
        }
    }
?>