-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 10-Nov-2020 às 01:51
-- Versão do servidor: 10.4.10-MariaDB
-- versão do PHP: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `aplicacao-gh`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_gh`
--

DROP TABLE IF EXISTS `contatos_gh`;
CREATE TABLE IF NOT EXISTS `contatos_gh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) NOT NULL,
  `telefone` varchar(15) NOT NULL,
  `email` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `contatos_gh`
--

INSERT INTO `contatos_gh` (`id`, `nome`, `telefone`, `email`) VALUES
(9, 'Leonardo Flores', '55991849847d', 'leonardo2505@hotmail.com'),
(8, 'd', '12314124123', 'leonardo2505@hotmail.com'),
(7, 'd', '12314124123', 'leonardo2505@hotmail.com'),
(6, 'Leonardo Flores', '12314124123', 'leonardo2505@hotmail.com'),
(10, 'Leonardo Flores', '55991849847d', 'leonardo2505@hotmail.com'),
(11, 'Leonardo Flores', '55991849847', 'leonardo2505@hotmail.com'),
(12, 'Leonardo Flores AA', '55991849847', 'teste1@gmail.com'),
(13, 'Leonardo Flores', '55991849847', 'leonardo2505@hotmail.com');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
